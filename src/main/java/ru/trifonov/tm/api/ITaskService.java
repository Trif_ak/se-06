package ru.trifonov.tm.api;

import ru.trifonov.tm.entity.Task;

import java.text.ParseException;
import java.util.Collection;

public interface ITaskService {
    void persist(String name, String projectId, String userId, String description, String beginDate, String endDate) throws ParseException;
    void merge(String name, String id, String projectId, String userId, String description, String beginDate, String endDate) throws ParseException;
    Task findOne(String id, String userId) throws Exception;
    Collection<Task> findAll(String projectId, String userId) throws Exception;
    void remove(String id, String userId);
    void removeAllOfProject(String projectId, String userId);
    void removeAllOfUser(String userId);
}
