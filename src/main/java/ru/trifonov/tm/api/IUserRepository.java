package ru.trifonov.tm.api;

import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;

public interface IUserRepository {
    void persist(User user);
    void merge(User user);
    void insert(String id, String login, String password, RoleType roleType);
    void update(String id, String login, String password, RoleType roleType);
    User findOne(String id) throws Exception;
    Collection<User> findAll() throws Exception;
    User findLogin(String login) throws Exception;
    User findPassword(User user, String PasswordMD5) throws Exception;
    void removeOne(String id);
    void removeAll();
}
