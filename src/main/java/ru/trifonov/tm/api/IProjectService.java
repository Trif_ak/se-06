package ru.trifonov.tm.api;

import ru.trifonov.tm.entity.Project;

import java.text.ParseException;
import java.util.Collection;

public interface IProjectService {
    void persist(String name, String userId, String description, String beginDate, String endDate) throws Exception;
    void merge(String name, String id, String userId, String description, String beginDate, String endDate) throws Exception;
    Project findOne(String id, String userId) throws Exception;
    Collection<Project> findAll(String userId) throws Exception;
    void removeOne(String id, String userId);
    void removeAll(String userId);
}
