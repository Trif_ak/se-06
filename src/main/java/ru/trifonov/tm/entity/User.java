package ru.trifonov.tm.entity;

import ru.trifonov.tm.enumerate.RoleType;

public final class User {
    private String id;
    private String login;
    private String passwordMD5;
    private RoleType roleType;

    public User() {
    }

    public User(String id, String login, String passwordMD5, RoleType roleType) {
        this.id = id;
        this.login = login;
        this.passwordMD5 = passwordMD5;
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + login +
                "  PASSWORD " + passwordMD5 +
                "  USER'S ROLE " + roleType.getRoleType();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordMD5() {
        return passwordMD5;
    }

    public void setPasswordMD5(String passwordMD5) {
        this.passwordMD5 = passwordMD5;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
