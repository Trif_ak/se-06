package ru.trifonov.tm.command.user;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserUpdatePasswordCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-updatePass";
    }

    @Override
    public String getDescription() {
        return ": update your password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PASSWORD]");
        final String id = serviceLocator.getCurrentUserID();
        final String login = serviceLocator.getCurrentUser().getLogin();
        System.out.println("Enter new password");
        final String password = serviceLocator.getInCommand().nextLine();
        serviceLocator.getUserService().update(id, login, password, serviceLocator.getCurrentUser().getRoleType());
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
