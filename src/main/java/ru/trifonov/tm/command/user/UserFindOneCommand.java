package ru.trifonov.tm.command.user;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserFindOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-findOne";
    }

    @Override
    public String getDescription() {
        return ": return select user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND USER]");
        final String id = serviceLocator.getCurrentUserID();
        System.out.println(serviceLocator.getUserService().findOne(id));
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
