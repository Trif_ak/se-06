package ru.trifonov.tm.command.user;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserAuthorizationCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "user-auth";
    }

    @Override
    public String getDescription() {
        return ": authorization in program";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER AUTHORIZATION]");
        System.out.println("Enter your LOGIN");
        final String login = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter your PASSWORD");
        final String password = serviceLocator.getInCommand().nextLine();
        serviceLocator.setCurrentUser(serviceLocator.getUserService().authorizationUser(login, password));
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return null;
    }
}
