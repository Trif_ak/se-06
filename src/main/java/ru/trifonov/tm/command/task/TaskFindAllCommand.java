package ru.trifonov.tm.command.task;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;

public final class TaskFindAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-findAll";
    }

    @Override
    public String getDescription() {
        return ": return all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL TASK]");
        System.out.println("Enter ID of project");
        final String projectId = serviceLocator.getInCommand().nextLine();
        final String userId = serviceLocator.getCurrentUserID();
        final Collection<Task> inputList = serviceLocator.getTaskService().findAll(projectId, userId);
        for (final Task task : inputList) {
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
