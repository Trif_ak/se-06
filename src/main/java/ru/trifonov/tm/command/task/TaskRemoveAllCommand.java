package ru.trifonov.tm.command.task;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class TaskRemoveAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-removeAllOfProject";
    }

    @Override
    public String getDescription() {
        return ": removeOne select tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE ALL TASK]");
        System.out.println("Enter ID of project");
        final String projectId = serviceLocator.getInCommand().nextLine();
        final String userId = serviceLocator.getCurrentUserID();
        serviceLocator.getTaskService().removeAllOfProject(projectId, userId);
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
