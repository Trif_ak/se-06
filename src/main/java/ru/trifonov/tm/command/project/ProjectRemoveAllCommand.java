package ru.trifonov.tm.command.project;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ProjectRemoveAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-removeAllOfProject";
    }

    @Override
    public String getDescription() {
        return ": removeOne all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE ALL PROJECT]");
        final String userId = serviceLocator.getCurrentUserID();
        serviceLocator.getTaskService().removeAllOfUser(userId);
        serviceLocator.getProjectService().removeAll(userId);
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
