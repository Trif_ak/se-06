package ru.trifonov.tm.command.project;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;

public final class ProjectFindAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-findAll";
    }

    @Override
    public String getDescription() {
        return ": return all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL PROJECT]");
        final Collection<Project> inputList = serviceLocator.getProjectService().findAll(serviceLocator.getCurrentUserID());
        for (final Project project : inputList) {
            System.out.println(project);
        }
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
